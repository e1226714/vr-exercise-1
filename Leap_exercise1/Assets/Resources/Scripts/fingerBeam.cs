﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using Leap.Unity;
using Leap.Unity.Interaction;

public class fingerBeam : MonoBehaviour
{
    [SerializeField]
    ExtendedFingerDetector point;

    [SerializeField]
    LineRenderer lineRenderer;

    private GameObject activeCube = null;

    private GameObject hittedCube;
    float distanceToCube;
    public Material Cube;
    public Material CubeSelected;
    public Material CubeReady;

    public Material Line;
    
    private bool isActive;   
    private bool cubeHitted;
    bool isFocused = false;
    bool wasRunning = false;

    private Finger pinkyFinger;
    private Finger ringFinger;
    private Finger middleFinger;
    private Finger indexFinger;
    private Finger thumb;
    
    
    

    void Start()
    {
        
        lineRenderer.enabled = false;
        lineRenderer.GetComponent<Renderer>().material = Line;
        lineRenderer.startWidth = 0.02f;
        lineRenderer.endWidth = 0.02f;
        isActive = false;
        distanceToCube = 0;

        cubeHitted = false;
        hittedCube = null;

        pinkyFinger = null;
        ringFinger = null;
        middleFinger = null;
        indexFinger = null;
        thumb = null;


    }
    void Update()
    {
        // get all fingers
        pinkyFinger = point.HandModel.GetLeapHand().Fingers[(int)Leap.Finger.FingerType.TYPE_PINKY];
        ringFinger = point.HandModel.GetLeapHand().Fingers[(int)Leap.Finger.FingerType.TYPE_RING];
        middleFinger = point.HandModel.GetLeapHand().Fingers[(int)Leap.Finger.FingerType.TYPE_MIDDLE];
        indexFinger = point.HandModel.GetLeapHand().Fingers[(int)Leap.Finger.FingerType.TYPE_INDEX];
        thumb = point.HandModel.GetLeapHand().Fingers[(int)Leap.Finger.FingerType.TYPE_THUMB];
     
        if (isActive)
        {
            // get direction and position of the index finger
            Vector3 tipPosition = indexFinger.TipPosition.ToVector3();
            Vector3 tipDirection = indexFinger.Direction.ToVector3();
            RaycastHit hit;

          // do the line,  starting from the tip position
            lineRenderer.enabled = true;
            lineRenderer.SetPositions(new Vector3[] { tipPosition, tipPosition + 5 * tipDirection });
            lineRenderer.GetComponent<Renderer>().material = Line;
            Ray fingerRay = new Ray(tipPosition, tipDirection);

            // check if thumb is extended
            if (thumb.IsExtended)
            {
                // check what was hitted with the line
                if (Physics.Raycast(tipPosition, tipDirection, out hit, 10) && hit.transform.tag == "cube")
                {
                    // set the cube material back to normal after selecting
                    if(activeCube != null && !hit.rigidbody.gameObject.Equals(activeCube))
                    {
                        activeCube.GetComponent<Renderer>().material = Cube;
                    }
                    
                    activeCube = hit.rigidbody.gameObject;
                    // change material of cube
                    activeCube.GetComponent<Renderer>().material = CubeReady;
                    // hold the distance between cube and tip position
                    distanceToCube = Vector3.Distance(activeCube.transform.position, tipPosition);
                }
                else
                {
                    if (activeCube != null)
                    {
                        activeCube.GetComponent<Renderer>().material = Cube;
                        activeCube = null;
                    }
                }

                isFocused = false;

            }
            else // grab the cube, if thumb is folded
            {
                
                if (activeCube != null)
                {
                    isFocused = true;
                    activeCube.GetComponent<Renderer>().material = CubeSelected;
                    activeCube.transform.position = tipPosition + tipDirection * distanceToCube;
                } else
                {
                    EndFingerRay();
                }

            }

            
        }

        // drop everything if any finger moves
        if (pinkyFinger.IsExtended || ringFinger.IsExtended || middleFinger.IsExtended || !indexFinger.IsExtended)
        {
            EndFingerRay();
        }

    }



    public void StartFingerRay()
    {
        
        if (thumb.IsExtended)
        {
            isActive = true;
        }

    }

    public void EndFingerRay()
    {
        isActive = false;
        if (activeCube != null)
        {
            activeCube.GetComponent<Renderer>().material = Cube;
            activeCube = null;
        }
        lineRenderer.enabled = false;
        isFocused = false;
        wasRunning = false;
           }

}


