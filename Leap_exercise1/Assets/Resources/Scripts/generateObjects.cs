﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap.Unity;
using Leap.Unity.Interaction;
using Leap;
using System;

public class generateObjects : MonoBehaviour
{

    [SerializeField]
    private PinchDetector leftHand;

    [SerializeField]
    private PinchDetector rightHand;

    [SerializeField]
    private Material Cube;

    [SerializeField]
    private GameObject everyCreatedCube;

    Vector3 posLeft;
    Vector3 posRight;


    Vector3 biggestCube;
    Vector3 smallestCube;

    
    private GameObject cube;


    void Start()
    {
        biggestCube = new Vector3(.4f, .4f, .4f);
        smallestCube = new Vector3(0.1f, 0.1f, 0.1f);
        cube = null;
        


    }


    void Update()
    {
        
        // Detect the Pinch
        if (leftHand.IsHolding)
        {
            // second hand is joining 
            if (rightHand.IsHolding)
            {

                // get positions of the pinches
                posLeft = leftHand.Position;
                posRight = rightHand.Position;

                // get the middle for the cubespawn
                Vector3 cubePosition = (posLeft + posRight) / 2;
                // do the scaling
                Vector3 scaling = VectorAbsolute(leftHand.Position - rightHand.Position);
                scaling = MinMaxVector(scaling);

                if (cube == null)
                {
                    
                    // creates a new cube
                    cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cube.GetComponent<Renderer>().material = Cube;
                    
                    // set scaling of cube
                    cube.transform.localScale = scaling;
                    // sets the position of the cube between the Hands
                    cube.transform.position = cubePosition;
                }
                else 
                {
                    // set scaling of cube
                    cube.transform.localScale = scaling;
                    // sets the position of the cube between the Hands
                    cube.transform.position = cubePosition;
                }
            }

        }
        if (leftHand.DidRelease || rightHand.DidRelease)
        {
            if (cube != null)
            {
                cube.tag = "cube";
                // attaches the InteractionBehaviour Script to each Cube.
                cube.AddComponent<InteractionBehaviour>();
                cube.transform.parent = everyCreatedCube.transform;

                cube = null;
            }
        }

    
    }



public Vector3 VectorAbsolute(Vector3 vector3)
{
    // let the vector be absolute 
    return new Vector3(Mathf.Abs(vector3.x), Mathf.Abs(vector3.y), Mathf.Abs(vector3.z));
}

public Vector3 MinMaxVector(Vector3 vector3)
{
        //check if cube is in the range of allowed cube sizes.
    return new Vector3(Mathf.Max(smallestCube.x, Mathf.Min(biggestCube.x, vector3.x)),
       Mathf.Max(smallestCube.y, Mathf.Min(biggestCube.y, vector3.y)),
       Mathf.Max(smallestCube.z, Mathf.Min(biggestCube.z, vector3.z)));
}
}
