﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using Leap.Unity;
using Leap.Unity.Interaction;

public class palmBeam : MonoBehaviour
{
    [SerializeField]
    ExtendedFingerDetector point;

    [SerializeField]
    LineRenderer lineRenderer;

    private GameObject activeCube = null;

    private GameObject hittedCube;
    float distanceToCube;
    public Material Cube;
    public Material CubeSelected;
    public Material CubeReady;

    public Material Line;

    private bool isActive;
    private bool cubeHitted;
    bool isFocused = false;
    bool wasRunning = false;

    private Finger pinkyFinger;
    private Finger ringFinger;
    private Finger middleFinger;
    private Finger indexFinger;
    private Finger thumb;




    void Start()
    {

        lineRenderer.enabled = false;
        lineRenderer.GetComponent<Renderer>().material = Line;
        lineRenderer.startWidth = 0.02f;
        lineRenderer.endWidth = 0.02f;
        isActive = false;
        distanceToCube = 0;

        cubeHitted = false;
        hittedCube = null;

        pinkyFinger = null;
        ringFinger = null;
        middleFinger = null;
        indexFinger = null;
        thumb = null;


    }
    void Update()
    {
        
        pinkyFinger = point.HandModel.GetLeapHand().Fingers[(int)Leap.Finger.FingerType.TYPE_PINKY];
        ringFinger = point.HandModel.GetLeapHand().Fingers[(int)Leap.Finger.FingerType.TYPE_RING];
        middleFinger = point.HandModel.GetLeapHand().Fingers[(int)Leap.Finger.FingerType.TYPE_MIDDLE];
        indexFinger = point.HandModel.GetLeapHand().Fingers[(int)Leap.Finger.FingerType.TYPE_INDEX];
        thumb = point.HandModel.GetLeapHand().Fingers[(int)Leap.Finger.FingerType.TYPE_THUMB];

        if (isActive)
        {
            Vector3 palmDirection = point.HandModel.GetLeapHand().PalmNormal.ToVector3();
            
            // get positon with offset to avoid hitting the hand
            Vector3 palmPosition = point.HandModel.GetLeapHand().PalmPosition.ToVector3() + 0.2f*palmDirection;
            
            RaycastHit hit2;


            lineRenderer.enabled = true;
            lineRenderer.SetPositions(new Vector3[] { palmPosition, palmPosition + 1 * palmDirection });
            lineRenderer.GetComponent<Renderer>().material = Line;
            Ray palmRay = new Ray(palmPosition, palmDirection);

            if (thumb.IsExtended)
            {

                if (Physics.Raycast(palmPosition, palmDirection, out hit2, 10) && hit2.transform.tag == "cube")
                {
                    if (activeCube != null && !hit2.rigidbody.gameObject.Equals(activeCube))
                    {
                        activeCube.GetComponent<Renderer>().material = Cube;
                    }
                    activeCube = hit2.rigidbody.gameObject;
                    activeCube.GetComponent<Renderer>().material = CubeReady;
                    distanceToCube = Vector3.Distance(activeCube.transform.position, palmPosition);
                }
                else
                {
                    if (activeCube != null)
                    {
                        activeCube.GetComponent<Renderer>().material = Cube;
                        activeCube = null;
                    }
                }

                isFocused = false;

            }
            else
            {

                if (activeCube != null)
                {
                    isFocused = true;
                    activeCube.GetComponent<Renderer>().material = CubeSelected;
                    activeCube.transform.position = palmPosition + palmDirection * distanceToCube;
                }
                else
                {
                    EndPalmRay();
                }

            }


        }

        if (pinkyFinger.IsExtended || ringFinger.IsExtended || middleFinger.IsExtended || !indexFinger.IsExtended)
        {
            EndPalmRay();
        }

    }



    public void StartPalmRay()
    {

        if (thumb.IsExtended)
        {
            isActive = true;
        }

    }

    public void EndPalmRay()
    {
        isActive = false;
        if (activeCube != null)
        {
            activeCube.GetComponent<Renderer>().material = Cube;
            activeCube = null;
        }
        lineRenderer.enabled = false;
        isFocused = false;
        wasRunning = false;
    }

}


